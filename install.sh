#!/usr/bin/env bash

# Persistent installation of signal-tools

# Exit on error
set -euo pipefail

# Installation of useful packages
required_packages=(gettext git wget)
should_install=0
for package in "${required_packages[@]}"; do
  if ! dpkg-query -W -f='${Status}' "${package}" 2>/dev/null | grep -vq "ok installed ${package}"; then
    should_install=1
  fi
done
if [ "${should_install}" -eq 1 ]; then
  echo -e "\033[33mInstallation of the packages '${required_packages[*]}'\033[0;0m"
  sudo sh -c "apt update && apt install --no-install-recommends -y ${required_packages[*]}"
fi

temp_dir="$(mktemp -d)"
# shellcheck disable=SC2064
trap "rm -rf ${temp_dir}" 0 1 2 15

repo_url="https://0xacab.org/jc7v/signal-tools.git"
echo -n "'${repo_url}'... "

git clone "${repo_url}" "${temp_dir}"
"${temp_dir}/src/signal-tools" --verbose
