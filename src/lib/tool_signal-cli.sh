#!/usr/bin/env bash

# Exit on error
set -euo pipefail

# shellcheck source-path=src/lib
source "$(dirname -- "${BASH_SOURCE[0]}")/utils.sh"

##### SIGNAL CLI #######################################################################################################
signal_cli__get_version() {
  result="${STR_NOT_FOUND}"
  version="$(find -L "${dir_bin}" -name "signal-cli-*" -type d 2>/dev/null | tail -n 1 | sed 's/^.*signal-cli-\(.*\)$/\1/')"
  if [ -n "${version}" ]; then
    if ! grep -Eq "^alias signal-cli='.*${SIGNAL_CLI_COMMAND}'$" "${BASHRC}"; then
      result="${STR_MISCONFIG}"
    else
      result="${version}"
    fi
  fi

  local tool_name="${STR_TOOL_CLI}"
  log 5 "$(eval_gettext $"Tool '${tool_name}' has currently the version '${result}'")"

  echo "${result}"
}

signal_cli__any() {
  local tool_name="${STR_TOOL_CLI}"
  log 3 "$(eval_gettext $"Installation of the tool '${tool_name}'")"

  install_if_necessary "openjdk-${openjdk_version}-jre"

  if [ ! -d "${DIR_SIGNAL_CLI}" ]; then
    SIGNAL_CLI_ARCHIVE="${DIR_DOWNLOADS}/signal-cli-${signal_cli_version}.tar.gz"
    # If the file has been already downloaded but is corrupted, removing it
    if [ -f "${SIGNAL_CLI_ARCHIVE}" ] && ! gunzip -t "${SIGNAL_CLI_ARCHIVE}" 2>/dev/null; then
      log 2 "$(eval_gettext $"Removing corrupted signal-cli archive to download it again")"
      rm -rf "${SIGNAL_CLI_ARCHIVE}"
    fi
    # Lazy download
    if [ ! -f "${SIGNAL_CLI_ARCHIVE}" ] || [ ! -s "${SIGNAL_CLI_ARCHIVE}" ]; then
      fail_if_no_internet_connection
      log 4 "$(eval_gettext $"Downloading Signal CLI v${signal_cli_version} into '${DIR_SIGNAL_CLI}'")"
      ${wget} -O "${SIGNAL_CLI_ARCHIVE}" -N "https://github.com/AsamK/signal-cli/releases/download/v${signal_cli_version}/signal-cli-${signal_cli_version}.tar.gz" || rm -f "${SIGNAL_CLI_ARCHIVE}"
    fi
    log 5 "$(eval_gettext $"Extracting '${SIGNAL_CLI_ARCHIVE}' to '${dir_bin}'")"
    tar -zxvf "${SIGNAL_CLI_ARCHIVE}" -C "${dir_bin}/"
  fi

  log 5 $"Setting variable JAVA_TOOL_OPTIONS and 'alias signal-cli' into ${BASHRC}"
  sed -i "/export JAVA_TOOL_OPTIONS='/d" "${BASHRC}"
  sed -i "/alias signal-cli='/d" "${BASHRC}"
  echo -e "export JAVA_TOOL_OPTIONS='${JAVA_TOOL_OPTIONS}'\nalias signal-cli='${SIGNAL_CLI_COMMAND}'" >>"${BASHRC}"

  # shellcheck disable=SC2317
  signal-cli() {
    ${SIGNAL_CLI_COMMAND} "$@"
  }
  export -f signal-cli
}

signal_cli__tails() {
  mkdir -p "${DOTFILES_PERSISTENT}/.local/share/signal-cli"

  # == Making it functional even after reboot == #
  # Pass if signal-cli is a symlink,
  if [ -L "${dir_bin}/signal-cli-${signal_cli_version}/bin/signal-cli" ]; then
    log 5 "$(eval_gettext $"'${dir_bin}/signal-cli-${signal_cli_version}' appears to have been installed before the last reboot")"
  else
    local tool_name="${STR_TOOL_CLI}"
    log 5 "$(eval_gettext $"Making '${tool_name}' functional even after Tails reboot")"
    rm -rf "${DIR_BIN_PERSISTENT}"/signal-cli-*
    cp -r "${dir_bin}/signal-cli-${signal_cli_version}" "${DIR_BIN_PERSISTENT}/"
    cp "${BASHRC}" "${DOTFILES_PERSISTENT}/"
  fi
}

signal_cli__new_account() {
  # TODO: rewrite this part
  return

  # shellcheck disable=SC2317
  if zenity --question --text $"Do you want to register a new Signal account?"; then
    # shellcheck disable=SC2317
    register_signal_account() {
      xdg-open "https://signalcaptchas.org/registration/generate" 2>/dev/null
      new_account_values="$(zenity --forms --title $"New Signal account" --text $"What are your account details?" --add-entry=$"Phone number" --add-entry="Captcha" --add-entry=$"PIN code" --add-entry=$"Profile name")"
      new_account_values="${new_account_values// /}"
      IFS='|' read -r phone_number captcha pin profile_name <<<"${new_account_values}"
      if [[ ${phone_number:0:1} == "0" ]]; then
        phone_number="+33${phone_number:1}"
      fi

      log 5 $"Trying to register phone number '${phone_number}' with ${STR_TOOL_CLI}"
      signal-cli -a "${phone_number}" register --captcha "${captcha}" || return 1
      code="$(zenity --entry --title=$"Verification code" --text=$"What is the verification code you get by SMS?")"
      signal-cli -a "${phone_number}" verify "${code}"

      signal-cli -a "${phone_number}" setPin "${pin}"
      signal-cli -a "${phone_number}" updateProfile --name "${profile_name}"
    }

    retry register_signal_account
  fi
}

signal_cli() {
  signal_cli__any

  if OS_is_TAILS; then
    signal_cli__tails
  elif OS_is_Debian; then
    :
  else
    fail_not_supported "${STR_TOOL_CLI}"
  fi

  log 3 "${STR_TOOL_CLI} v$(signal_cli__get_version) $(gettext $"installed")"
  local tool_name="signal-cli"
  log 4 "$(eval_gettext $"To have the alias '${tool_name}' working, please run '${S_BOLD}source ~/.bashrc${RESET}'")"

  # Does the user want to configure a new account now?
  signal_cli__new_account
}

signal_cli__update() {
  local tool_name="${STR_TOOL_CLI}"
  log 3 "$(eval_gettext $"Updating the tool '${tool_name}'")"

  signal_cli
}

signal_cli__clean() {
  local remove_user_data="${1:?$(usage "<remove_user_data>")}"

  tool_name="${STR_TOOL_CLI}"
  log 3 "$(eval_gettext $"cleaning tool '${tool_name}'")"

  rm -rf "${DIR_SIGNAL_CLI}"
  sed -i "/export JAVA_TOOL_OPTIONS='/d" "${BASHRC}"
  sed -i "/alias signal-cli='/d" "${BASHRC}"

  if OS_is_TAILS; then
    rm -rf "${DIR_BIN_PERSISTENT}"/signal-cli-*
    cp "${BASHRC}" "${DOTFILES_PERSISTENT}/"
  fi

  local data_dir="${dir_config}/signal-cli"
  if [ -d "${data_dir}" ]; then
    log 4 "$(eval_gettext $"Removing CLI user data located in '${data_dir}'")"
    if [ "${remove_user_data}" = "true" ]; then
      rm -rf "${data_dir}"
    else
      log 4 "$(eval_gettext $"The data located in '${data_dir}' were not removed")"
    fi
  fi
}
