#!/usr/bin/env bash

# Exit on error
set -euo pipefail

# shellcheck source-path=src/lib
source "$(dirname -- "${BASH_SOURCE[0]}")/utils.sh"

##### SIGNAL DESKTOP ###################################################################################################
signal_desktop__get_version() {
  result="${STR_NOT_FOUND}"
  if OS_is_TAILS && [ -d "${LEGACY_SIGNAL_PATH}" ]; then
    result="${STR_LEGACY}"
  else
    result="$(package_version "signal-desktop")"
  fi

  tool_name="${STR_TOOL_DESKTOP}"
  log 5 "$(eval_gettext $"Tool '${tool_name}' has currently the version '${result}'")"

  echo "${result}"
}

signal_desktop__any() {
  local tool_name="${STR_TOOL_DESKTOP}"
  log 3 "$(eval_gettext $"Installation of the tool '${tool_name}'")"

  # GPG keyring
  local signal_keyring="${DIR_KEYRINGS}/signal-desktop-keyring.gpg"
  if [ ! -f "${signal_keyring}" ] || [ ! -s "${signal_keyring}" ]; then
    fail_if_no_internet_connection

    log 4 "$(gettext $"Adding Signal GPG key to keyrings")"

    # Run _sudo at least once before piping to don't use 'gpg --dearmor' result as password input
    _sudo whoami >/dev/null
    ${wget} -O- https://updates.signal.org/desktop/apt/keys.asc 2>/dev/null | gpg --dearmor | _sudo_tee "${signal_keyring}" >/dev/null || _sudo rm -f "${signal_keyring}"
  fi

  # APT source
  local apt_source_signal="${DIR_APT_SOURCES}/signal-xenial.list"
  source="deb [arch=$(dpkg --print-architecture) signed-by=${signal_keyring}] ${signal_desktop_repo_url} xenial main"
  if [ ! -e "${apt_source_signal}" ] || ! grep -Fq "${source}" "${apt_source_signal}"; then
    log 4 "$(eval_gettext $"Adding the new APT source"" '${apt_source_signal}'")"

    # Run _sudo at least once before piping to don't use 'echo -e' result as password input
    _sudo whoami >/dev/null
    echo -e "${source}" | _sudo_tee "${apt_source_signal}"
  fi
}

signal_desktop__debian() {
  fail_if_no_internet_connection

  log 4 "$(gettext $"Updating packages list")"
  _sudo_apt_update

  install_if_necessary signal-desktop

  log 3 "${STR_TOOL_DESKTOP} v$(signal_desktop__get_version) $(gettext $"installed")"
}

signal_desktop__tails() {
  # Removing previous desktop files
  rm -f "${DIR_APPS}/signal-tails.desktop" "${DIR_APPS_PERSISTENT}/signal-tails.desktop"
  local desktop_name="signal-tails-${OS_NAME}.desktop"
  rm -f "${DIR_APPS}/${desktop_name}"

  export SIGNALID="${OS_NAME}"
  envsubst <"${DIR_FILES}/Desktop/signal-tails-template.desktop" >"${DIR_APPS}/${desktop_name}"
  mkdir -p "${dir_config}/Signal-${OS_NAME}"

  # == Making it functional even after reboot == #
  local tool_name="${STR_TOOL_DESKTOP}"
  log 5 "$(eval_gettext $"Making '${tool_name}' functional even after Tails reboot")"
  cp "${DIR_APPS}/${desktop_name}" "${DIR_APPS_PERSISTENT}/"

  # Backup of Signal Desktop executable in case of APT errors after reboot
  if [ ! -d "${dir_bin}/Signal" ] && [ -d /opt/Signal ]; then
    cp -r /opt/Signal/ "${dir_bin}/"
  fi

  # 'apt update && apt upgrade' working
  if [ ! -f "${DIR_KEYRINGS_PERSISTENT}/signal-desktop-keyring.gpg" ] ||
    [ ! -f "${DIR_APT_SOURCES_PERSISTENT}/signal-xenial.list" ] ||
    [ ! -f "${DIR_DPKG_HOOKS}/90remove-signal-desktop-desktop-file" ] ||
    [ ! -f "${DIR_DPKG_HOOKS_PERSISTENT}/90remove-signal-desktop-desktop-file" ] ||
    [ -f "${SIGNAL_DESKTOP_DESKTOP_FILE}" ]; then
    log 4 "$(gettext $"Making APT sources files persistent to have fluent 'apt upgrade' even after reboot")"
    _sudo cp -n "${DIR_KEYRINGS}/signal-desktop-keyring.gpg" "${DIR_KEYRINGS_PERSISTENT}/"
    _sudo cp -n "${DIR_APT_SOURCES}/signal-xenial.list" "${DIR_APT_SOURCES_PERSISTENT}/"
    _sudo cp "${DIR_FILES}/dpkg-hooks/90remove-signal-desktop-desktop-file" "${DIR_DPKG_HOOKS}/"
    _sudo cp "${DIR_FILES}/dpkg-hooks/90remove-signal-desktop-desktop-file" "${DIR_DPKG_HOOKS_PERSISTENT}/"
    _sudo rm -f "${SIGNAL_DESKTOP_DESKTOP_FILE}"
  fi

  if [[ "$(signal_desktop__get_version)" == "${STR_LEGACY}" ]]; then
    log 4 "$(eval_gettext $"Legacy '${STR_TOOL_DESKTOP}' installation found, migration to the new system")"

    # Temporary backing up user data in case of loss
    mkdir -p /tmp/backups
    cp -r "${DOTFILES_PERSISTENT}/.config"/Signal* /tmp/backups/ || true
    # Moving user data
    find -L "${DOTFILES_PERSISTENT}/.config" -name "Signal-*" -exec mv {} "${dir_config}/" \; 2>/dev/null || true
    local path_signal="${DOTFILES_PERSISTENT}/.config/Signal"
    if [ -d "${path_signal}" ]; then
      mv "${path_signal}" "${dir_config}/Signal"
    fi

    rmdir "${DOTFILES_PERSISTENT}/.config/" || true

    # Moving desktop files
    while IFS= read -r -d '' desktop_file; do
      instance="$(basename "${desktop_file}" | sed 's/Signal-\(.*\)\.desktop/\1/')"
      log 5 "$(eval_gettext $"Migration of the instance '${instance}'")"

      mkdir -p "${dir_config}/Signal-${instance}"

      desktop_name="${DIR_APPS}/signal-tails-${instance}.desktop"
      export SIGNALID="${instance}"
      envsubst <"${DIR_FILES}/Desktop/signal-${OS_NAME,,}-template.desktop" >"${desktop_name}"
      cp "${desktop_name}" "${DIR_APPS_PERSISTENT}/"
      rm -f "${desktop_file}" "${DIR_APPS_PERSISTENT}/$(basename "${desktop_file}")"
    done < <(find -L "${DIR_APPS}" -name 'Signal-*.desktop' -print0)

    # Removing old installation
    log 5 "$(eval_gettext $"Removing the old installation of '${STR_TOOL_DESKTOP}'")"
    rm -rf "${DIR_BIN_PERSISTENT}/signal-desktop"
    mv "${LEGACY_SIGNAL_PATH}" /tmp/signal-desktop-legacy
  fi
}

signal_desktop() {
  signal_desktop__any

  if OS_is_Debian; then
    signal_desktop__debian
  elif OS_is_TAILS; then
    signal_desktop__tails
    signal_desktop__debian
  else
    fail_not_supported "${STR_TOOL_DESKTOP}"
  fi
}

signal_desktop__update() {
  local tool_name="${STR_TOOL_DESKTOP}"
  log 3 "$(eval_gettext $"Updating the tool '${tool_name}'")"

  _sudo_apt_update
  _sudo apt install --only-upgrade signal-desktop
}

signal_desktop__clean() {
  local tool_name="${STR_TOOL_DESKTOP}"
  log 3 "$(eval_gettext $"cleaning tool '${tool_name}'")"

  local signal_keyring="${DIR_KEYRINGS}/signal-desktop-keyring.gpg"
  local apt_source_signal="${DIR_APT_SOURCES}/signal-xenial.list"
  if [ -f "${signal_keyring}" ] ||
    [ -f "${DIR_KEYRINGS_PERSISTENT}/signal-desktop-keyring.gpg" ] ||
    [ -f "${apt_source_signal}" ] ||
    [ -f "${DIR_APT_SOURCES_PERSISTENT}/signal-xenial.list" ]; then
    log 5 "$(eval_gettext $"Cleaning APT stuff")"
    _sudo rm -f "${signal_keyring}" "${DIR_KEYRINGS_PERSISTENT+not_found}/signal-desktop-keyring.gpg" "${apt_source_signal}" "${DIR_APT_SOURCES_PERSISTENT+not_found}/signal-xenial.list"
  fi

  remove_if_necessary signal-desktop

  if OS_is_TAILS; then
    rm -f "${DIR_APPS}/signal-tails.desktop"
    rm -f "${DIR_APPS_PERSISTENT}/signal-tails.desktop"
  fi
}
