#!/usr/bin/env bash

# This file regroups all the functions used by other scripts

# Exit on error
set -euo pipefail

###############
# PRETTY LOGS #
###############

# shellcheck disable=SC2034
{
  RESET='\033[0;0m'

  # Styles
  S_NORMAL='\033[0m'
  S_BOLD='\033[1m'
  S_DIM='\033[2m'
  S_ITALIC='\033[3m'
  S_UNDERLINE='\033[4m'
  S_BLINK='\033[5m'
  S_REVERSE='\033[7m'
  S_INVISIBLE='\033[8m'

  # Colors
  C_BLACK='\033[30m'
  C_RED='\033[31m'
  C_GREEN='\033[32m'
  C_YELLOW='\033[33m'
  C_BLUE='\033[34m'
  C_PURPLE='\033[35m'
  C_CYAN='\033[36m'
  C_GREY='\033[37m'
}

USAGE_FUNCNAME_INDEX=2
usage() {
  local funcname="${FUNCNAME[${USAGE_FUNCNAME_INDEX}]}"
  if [[ ${funcname} == "main" ]]; then
    funcname="$0"
  fi
  echo -e "${S_BOLD}Usage${RESET}: ${S_UNDERLINE}${funcname}${RESET} ${C_BLUE}$*${RESET}"
}

declare -A LOG_LEVELS
# https://en.wikipedia.org/wiki/Syslog#Severity_level
LOG_LEVELS=(
  [1]="${C_RED}✖ err "
  [2]="${C_YELLOW}➜ warn"
  [3]="${C_GREEN}✔ note"
  [4]="${C_BLUE}~ info"
  [5]="${C_GREY} debug"
)
log() {
  local level=${1:?$(usage "<log_level> [msg...]")}
  shift
  if [ "${__VERBOSE}" -ge "${level}" ]; then
    echo -e "[${LOG_LEVELS["${level}"]}${RESET}]" "$@" >&2
    if ${NOTIFICATION}; then
      zenity --notification --title "${LOG_LEVELS["${level}"]}" --text "$@"
    fi
  fi
}

#############
# CONSTANTS #
#############

# Remove unused variables warnings
# shellcheck disable=SC2034
{
  DIR_ROOT=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")/../.." &>/dev/null && pwd)
  DIR_SRC="${DIR_ROOT}/src"

  # Internationalization
  if [ -z "${LC_ALL+x}" ]; then
    export LC_ALL="${LANG}"
  fi
  TEXTDOMAIN=messages
  TEXTDOMAINDIR="${DIR_SRC}/translations"
  # shellcheck source=/usr/bin/gettext.sh
  source gettext.sh

  DIR_FILES="${DIR_SRC}/files"
  DIR_DOWNLOADS="$(xdg-user-dir DOWNLOAD)"
  DIR_APPS="${HOME}/.local/share/applications"
  dir_config="${HOME}/.config"
  DIR_APT_SOURCES="/etc/apt/sources.list.d"
  DIR_DPKG_HOOKS="/etc/apt/apt.conf.d"
  DIR_KEYRINGS="/usr/share/keyrings"
  DIR_APT_LISTS_CACHE="/var/lib/apt/lists"
  BASHRC="${HOME}/.bashrc"
  SIGNAL_DESKTOP_DESKTOP_FILE="/usr/share/applications/signal-desktop.desktop"

  OS_NAME="$(awk -F= '/^NAME/{print $2}' /etc/os-release)"
  OS_NAME="${OS_NAME//\"/}"
  [[ ${OS_NAME} == "Debian GNU/Linux" ]] && OS_NAME="Debian"

  STR_INSTALLED="$(gettext $"installed")"
  STR_NOT_FOUND="$(gettext $"NOT FOUND")"
  STR_MISCONFIG="$(gettext $"MISCONFIG")"
  STR_LEGACY="$(gettext $"LEGACY")"
  STR_TOOL_SIGNAL_TOOLS="$(gettext $"Signal Tools")"
  STR_TOOL_DESKTOP="$(gettext $"Signal Desktop")"
  STR_TOOL_CLI="$(gettext $"Signal CLI")"
  STR_TOOL_MULTI_INSTANCES="$(gettext $"Additional Signal instances")"
  STR_TOOL_CAPTCHAS="$(gettext $"Signal Captchas Handler")"
  STR_CAPTCHA_HANDLER_NAME="signal-captcha-handler"

  # Variables that can change in function of the OS
  signal_cli_version="0.13.7"
  openjdk_version="21"
  signal_desktop_repo_url="https://updates.signal.org/desktop/apt"
  dir_bin="${HOME}/bin"
  prefix_network=""
  wget="wget"

  if [[ ${OS_NAME} == "Debian" ]]; then
    openjdk_version="17"
  fi

  if [[ ${OS_NAME} == "Tails" ]]; then
    # shellcheck source=/dev/null
    source /usr/local/lib/tails-shell-library/systemd.sh
    dir_bin="${HOME}/Persistent/bin"
    dir_config="${HOME}/Persistent/.config"

    # FIXME: v0.13.* when 'openjdk-21-jre' becomes available
    signal_cli_version="0.12.8"
    openjdk_version="17"
    signal_desktop_repo_url="tor+${signal_desktop_repo_url}"

    prefix_network="torsocks "
    wget="torsocks wget"

    # Specific TAILS variables
    __DIR_TAILS_PERSISTENCE="/live/persistence/TailsData_unlocked"
    DIR_KEYRINGS_PERSISTENT="${__DIR_TAILS_PERSISTENCE}/keyrings"
    DIR_APT_SOURCES_PERSISTENT="${__DIR_TAILS_PERSISTENCE}/apt-sources.list.d"
    DIR_DPKG_HOOKS_PERSISTENT="${__DIR_TAILS_PERSISTENCE}/apt.conf.d"
    DOTFILES_PERSISTENT="${__DIR_TAILS_PERSISTENCE}/dotfiles"
    DIR_BIN_PERSISTENT="${DOTFILES_PERSISTENT}/Applications"
    DIR_APPS_PERSISTENT="${DOTFILES_PERSISTENT}/.local/share/applications"
    # Previous installation script variables from 'code/install.sh' to allow migration
    LEGACY_SIGNAL_PATH="${HOME}/Applications/signal-desktop"

    # Checking persistent options
    PERSISTENCE_CONF_PERSISTENT="${__DIR_TAILS_PERSISTENCE}/persistence.conf"
    PERSISTENCE_CONF="/tmp/persistence.conf"
  fi

  DIR_SIGNAL_CLI="${dir_bin}/signal-cli-${signal_cli_version}"
  SIGNAL_CLI_COMMAND="${prefix_network}${DIR_SIGNAL_CLI}/bin/signal-cli --config ${dir_config}/signal-cli"
  export JAVA_TOOL_OPTIONS='-Djava.net.preferIPv4Stack=true'

}

#########
# UTILS #
#########

fail_with() {
  log 1 "$*" >&2
  exit 1
}

fail_not_supported() {
  local tool_not_supported="${1:?$(usage "<tool_name>")}"
  fail_with "$(eval_gettext $"Not yet supported: '${tool_not_supported}' install from '${OS_NAME}'.")"
}

fail_activate_persistent() {
  local to_activate="${1:?$(usage "<to_activate>")}"
  fail_with "$(eval_gettext $"Open 'Applications > Tails > Persistent Storage' and activate '${to_activate}'")"
}

fail_if_no_internet_connection() {
  if OS_is_TAILS; then
    if ! tor_has_bootstrapped; then
      fail_with "$(gettext $"This script requires a connection to Tor")"
    fi
  else
    if ! wget -q --spider 1.1.1.1; then
      fail_with "$(gettext $"Please activate your Internet connection")"
    fi
  fi
  log 5 "$(gettext $"The device has a working Internet connection")"
}

package_exists() {
  local package="${1:?$(usage "<package_name>"))}"

  dpkg-query -W -f='${Status}' "${package}" 2>/dev/null | grep -vq "ok installed ${package}"
}

package_version() {
  local package="${1:?$(usage "<package_name>")}"

  dpkg -s "${package}" 2>/dev/null | grep Version | cut -d ' ' -f 2 || echo "${STR_NOT_FOUND}"
}

_sudo_apt_update() {
  # Run _sudo at least once
  _sudo whoami >/dev/null

  log 4 "$(gettext $"Running 'apt update'")"
  local retry="false"

  temp_file="$(mktemp -p "${dir_bin}")"
  # shellcheck disable=SC2064
  trap "rm -f ${temp_file}" 0 1 2 15

  _sudo apt update >/dev/null 2>"${temp_file}" || retry="true"

  if ${retry}; then
    log 2 "$(gettext $"'apt update' just failed, retrying. Maybe you don't have enough space on your device.")"

    if grep -q "$(gettext $"No space left on device")" "${temp_file}"; then
      fail_with "$(gettext $"You don't have enough space left on your device")"
    fi

    if grep -Eq "Splitting up ${DIR_APT_LISTS_CACHE}/.* into data and signature failed" "${temp_file}"; then
      _sudo rm -rf "${DIR_APT_LISTS_CACHE}"/*
      _sudo apt-get clean
    fi

    _sudo apt update
  fi
}

install_if_necessary() {
  local package="${1:?$(usage "<package_name>"))}"

  if ! package_exists "${package}"; then
    fail_if_no_internet_connection

    # Test if the 'apt update' has been run at least once
    if [ -z "$(ls -A "${DIR_APT_LISTS_CACHE}")" ]; then
      log 4 "$(gettext $"Updating packages list")"
      _sudo_apt_update
    fi

    log 3 "$(eval_gettext $"Installing package '${package}'")"
    _sudo apt-get install -y "${package}"
  fi
}

remove_if_necessary() {
  local package="${1:?$(usage "<package_name>"))}"

  if package_exists "${package}"; then
    log 3 "$(eval_gettext $"Removing package '${package}'")"
    _sudo apt-get remove -y "${package}"
  else
    log 5 "$(eval_gettext $"The package '${package}' is not installed, do not remove it")"
  fi
}

checkbox_state() {
  local version="${1:?$(usage "<version>")}"
  if [[ ${version} == "${STR_NOT_FOUND}" ]] || [[ ${version} == "${STR_MISCONFIG}" ]] || [[ ${version} == "${STR_LEGACY}" ]]; then
    echo TRUE
  else
    echo FALSE
  fi
}

retry() {
  local n=1
  local max=5
  local delay=5
  while true; do
    { "$@" && break; } || {
      if [[ ${n} -lt ${max} ]]; then
        log 2 "$(eval_gettext $"Command failed. Attempt ${n} / ${max}: ")$*"
        ((n++))
        sleep "${delay}"
      else
        fail_with "$(eval_gettext $"This command failed after ${max} attempts: ")$*"
      fi
    }
  done
}

OS_is_TAILS() {
  [[ ${OS_NAME} == "Tails" ]]
}

# Ubuntu is based on Debian, same installation process
OS_is_Debian() {
  [[ ${OS_NAME} == "Debian" ]] || [[ ${OS_NAME} == "Ubuntu" ]]
}

#OS_is_Fedora() {
#  [[ ${OS_NAME} == "Fedora Linux" ]]
#}

user_password=""
_get_password() {
  if ${NOTIFICATION}; then
    user_password="$(zenity --password)"
  else
    echo -n "[sudo] $(gettext $"password for") $(whoami): " >&2

    while IFS= read -r -s -n 1 letter; do
      # Enter key
      [[ ${letter} == $'\0' ]] && break

      user_password="${user_password}${letter}"

      echo -n "*" >&2
    done
    echo
  fi
  echo "${user_password}" | sudo -S whoami >/dev/null 2>&1 || {
    user_password=""
    return 1
  }
}

_sudo() {
  if [ -z "${user_password}" ]; then
    log 4 "$(gettext $"Obtaining the user password once")"
    retry _get_password
  fi

  echo "${user_password}" | sudo -S "$@" 2> >(sed "s/\[sudo\] $(gettext $"password for") $(whoami) \?: //" >&2)
}

# Broken pipe with 'gpg --dearmor | _sudo tee', this function corrects it
_sudo_tee() {
  local destination="${1:?$(usage "<destination>")}"

  temp_file="$(mktemp)"
  # shellcheck disable=SC2064
  trap "rm -f ${temp_file}" 0 1 2 15

  tee "${temp_file}" >/dev/null
  _sudo cp --no-preserve=mode "${temp_file}" "${destination}"
}

##################
# INITIALISATION #
##################

bootstrap() {
  log 5 "$(eval_gettext $"Bootstrapping for the device '${OS_NAME}'")"
  #if package_exists "gettext"; then
  #  # Translate all strings according to user preference
  #  msgfmt "${TEXTDOMAINDIR}/fr/LC_MESSAGES/${TEXTDOMAIN}.po" -o "${TEXTDOMAINDIR}/fr/LC_MESSAGES/${TEXTDOMAIN}.mo"
  #fi

  if OS_is_TAILS; then
    if [ ! -f "${PERSISTENCE_CONF}" ]; then
      # copy to work on it without prompting su access a lot
      log 3 "$(gettext $"Checking persistent options")"
      _sudo install -o amnesia -g amnesia -t /tmp "${PERSISTENCE_CONF_PERSISTENT}"
    fi
    # Persistent Folder
    if ! grep -q "source=Persistent" "${PERSISTENCE_CONF}"; then
      rm -f "${PERSISTENCE_CONF}"
      fail_activate_persistent "$(gettext $"Persistent Folder")"
    fi
    # Additional softwares
    if ! grep -q "source=apt/cache" "${PERSISTENCE_CONF}" || ! grep -q "source=apt/lists" "${PERSISTENCE_CONF}"; then
      rm -f "${PERSISTENCE_CONF}"
      fail_activate_persistent "$(gettext $"Additional Software")"
    fi
    # Dotfiles
    if ! grep -q "source=dotfiles,link" "${PERSISTENCE_CONF}"; then
      rm -f "${PERSISTENCE_CONF}"
      fail_activate_persistent "$(gettext $"Dotfiles")"
    fi
    # Keyrings persistent
    if ! grep -q "source=keyrings" "${PERSISTENCE_CONF}"; then
      log 4 "$(gettext $"Configuration of persistent keyrings")"
      echo "${DIR_KEYRINGS}  source=keyrings" >>"${PERSISTENCE_CONF}"
      _sudo cp "${PERSISTENCE_CONF}" "${PERSISTENCE_CONF_PERSISTENT}"
      _sudo install -d -m 755 "${DIR_KEYRINGS_PERSISTENT}"
    fi
    # APT sources persistent
    if ! grep -q "source=apt-sources.list.d,link" "${PERSISTENCE_CONF}"; then
      log 4 "$(gettext $"Configuration of persistent APT sources")"
      echo "${DIR_APT_SOURCES}  source=apt-sources.list.d,link" >>"${PERSISTENCE_CONF}"
      _sudo cp "${PERSISTENCE_CONF}" "${PERSISTENCE_CONF_PERSISTENT}"
      _sudo install -d -m 755 "${DIR_APT_SOURCES_PERSISTENT}"
    fi
    # DPKG hooks, used at startup to configure some stuff
    if ! grep -q "source=apt.conf.d,link" "${PERSISTENCE_CONF}"; then
      log 4 "$(gettext $"Configuration of persistent DPKG hooks")"
      echo "${DIR_DPKG_HOOKS}  source=apt.conf.d,link" >>"${PERSISTENCE_CONF}"
      _sudo cp "${PERSISTENCE_CONF}" "${PERSISTENCE_CONF_PERSISTENT}"
      _sudo install -d -m 755 "${DIR_DPKG_HOOKS_PERSISTENT}"
    fi

    mkdir -p "${dir_config}" "${DIR_APPS}" "${DIR_APPS_PERSISTENT}" "${DIR_BIN_PERSISTENT}" "${dir_bin}"
  elif OS_is_Debian; then
    :
  else
    fail_not_supported "OS"
  fi

  mkdir -p "${dir_bin}" "${DIR_APPS}"
}
