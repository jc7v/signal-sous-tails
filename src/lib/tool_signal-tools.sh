#!/usr/bin/env bash

# Exit on error
set -euo pipefail

# shellcheck source-path=src/lib
source "$(dirname -- "${BASH_SOURCE[0]}")/utils.sh"

_signal_tools_path="${DIR_ROOT}"
str_path_signal_tools="export PATH=${dir_bin}/signal-tools/src:\$PATH"

signal_tools__get_remote() {
  local signal_tools_repo_url="https://0xacab.org/jc7v/signal-tools"

  _signal_tools_path="$(mktemp -d)"
  # shellcheck disable=SC2064
  trap "rm -rf ${_signal_tools_path}" 0 1 2 15
  log 4 $"Cloning fresh signal-tools code from '${signal_tools_repo_url}'"

  fail_if_no_internet_connection
  install_if_necessary git
  git clone "${signal_tools_repo_url}" "${dir_bin}/signal-tools" >/dev/null
  log 5 $"Signal tools cloned into '${dir_bin}/signal-tools'"
}

##### SIGNAL TOOLS #####################################################################################################
signal_tools__get_version() {
  result="${STR_NOT_FOUND}"
  if [ -s "${DIR_APPS}/signal-tools.desktop" ] && [ -x "${dir_bin}/signal-tools/src/signal-tools" ]; then
    result="${STR_INSTALLED}"
  fi

  local tool_name="${STR_TOOL_SIGNAL_TOOLS}"
  log 5 "$(eval_gettext $"Tool '${tool_name}' has currently the version '${result}'")"

  echo "${result}"
}

signal_tools__any() {
  local tool_name="${STR_TOOL_SIGNAL_TOOLS}"

  if [[ ${DIR_ROOT} == "${dir_bin}/signal-tools" ]]; then
    log 4 $"${STR_TOOL_SIGNAL_TOOLS} already installed into '${dir_bin}'"
  else
    log 3 "$(eval_gettext $"Installation of the tool '${tool_name}'")"
    log 5 "$(eval_gettext $"Copying signal-tools from '${_signal_tools_path}' to '${dir_bin}'")"

    cp "${_signal_tools_path}/src/files/Desktop/signal-tools.desktop" "${DIR_APPS}/"

    rm -rf "${dir_bin}/signal-tools"
    if [ ! -d "${DIR_ROOT}/.git" ]; then
      # Code probably obtained from a ZIP archive, missing git stuff
      signal_tools__get_remote
    else
      cp -r "${_signal_tools_path}" "${dir_bin}/signal-tools"
    fi

    log 5 $"Adding signal-tools path into ~/.bashrc"
    sed -i "/^export PATH=.*signal-tools/d" "${BASHRC}"
    echo -e "${str_path_signal_tools}" >>"${BASHRC}"
  fi
}

signal_tools__tails() {
  local path_signal_tools="${dir_bin}/signal-tools/src/signal-tools"
  sed -i "s#^Exec=.*#Exec=${path_signal_tools} --gui#g" "${DIR_APPS}/signal-tools.desktop"

  # == Making it functional even after reboot == #
  # Pass if signal-tools is a symlink,
  if [ -L "${path_signal_tools}" ]; then
    log 5 "$(eval_gettext $"'${path_signal_tools}' appears to have been installed before the last reboot")"
  else
    local tool_name="${STR_TOOL_SIGNAL_TOOLS}"
    log 5 "$(eval_gettext $"Making '${tool_name}' functional even after Tails reboot")"
    cp "${DIR_APPS}/signal-tools.desktop" "${DIR_APPS_PERSISTENT}/"
    rm -rf "${DIR_BIN_PERSISTENT}/signal-tools"
    cp -r "${_signal_tools_path}" "${DIR_BIN_PERSISTENT}/signal-tools"
    cp "${BASHRC}" "${DOTFILES_PERSISTENT}/"
  fi
}

signal_tools() {
  signal_tools__any

  if OS_is_Debian; then
    :
  elif OS_is_TAILS; then
    signal_tools__tails
  else
    fail_not_supported "${STR_TOOL_SIGNAL_TOOLS}"
  fi

  log 3 "${STR_TOOL_SIGNAL_TOOLS} $(gettext $"installed")"
  tool_name="signal-tools"
  log 4 "$(eval_gettext $"To have the alias '${tool_name}' working, please run '${S_BOLD}source ~/.bashrc${RESET}'")"
}

signal_tools__update() {
  local tool_name="${STR_TOOL_SIGNAL_TOOLS}"
  log 3 "$(eval_gettext $"Updating the tool '${tool_name}'")"

  cd "${dir_bin}/signal-tools"
  git checkout main
  fail_if_no_internet_connection
  git pull

  if OS_is_TAILS; then
    if [ ! -d "${DIR_BIN_PERSISTENT}/signal-tools" ]; then
      cp -r "${_signal_tools_path}" "${DIR_BIN_PERSISTENT}/signal-tools"
    fi
    cd "${DIR_BIN_PERSISTENT}/signal-tools"
    git checkout main
    git pull
  fi
}

signal_tools__clean() {
  local tool_name="${STR_TOOL_SIGNAL_TOOLS}"
  log 3 "$(eval_gettext $"cleaning tool '${tool_name}'")"

  rm -f "${DIR_APPS}/signal-tools.desktop"
  rm -rf "${dir_bin}/signal-tools"
  sed -i "#${str_path_signal_tools}#d" "${BASHRC}"

  if OS_is_TAILS; then
    rm -f "${DIR_APPS_PERSISTENT}/signal-tools.desktop"
    rm -rf "${DIR_BIN_PERSISTENT}/signal-tools"
    cp "${BASHRC}" "${DOTFILES_PERSISTENT}/"
  fi
}
