#!/usr/bin/env bash

# Exit on error
set -euo pipefail

# shellcheck source-path=src/lib
source "$(dirname -- "${BASH_SOURCE[0]}")/utils.sh"

##### SIGNAL MULTI INSTANCES ###########################################################################################
_signal_multi_instances__list_data_dir() {
  find -L "${DIR_APPS}" -maxdepth 1 -type f -name "signal-${OS_NAME,,}-*.desktop" | sort | sed "s#${DIR_APPS}/signal-${OS_NAME,,}-\(.*\).desktop#\1#" || true
}

_signal_multi_instances__list_removed_instances() {
  temp_file="$(mktemp)"
  # shellcheck disable=SC2064
  trap "rm -f ${temp_file}" 0 1 2 15
  find -L "${dir_config}" -maxdepth 1 -type d -name "Signal*" | grep -v "Signal-${OS_NAME}" | sort >"${temp_file}"
  for instance in $(_signal_multi_instances__list_data_dir); do
    if grep -q "${dir_config}/Signal-${instance}" "${temp_file}"; then
      sed -i "/Signal-${instance}$/d" "${temp_file}"
    fi
  done
  cat "${temp_file}"
}

_signal_multi_instances__sanitizer() {
  USAGE_FUNCNAME_INDEX=1
  local input="${1:?$(usage "<instance_name>")}"

  # Removing trailing spaces
  sanitized="${input//Signal /}"
  sanitized="${sanitized// /}"
  sanitized="${sanitized^}"

  echo "${sanitized}"
}

# List all active and deleted instances. Could remove user data of removed instances.
signal_multi_instances__list_instances() {
  USAGE_FUNCNAME_INDEX=1
  local remove_user_data="${1:?$(usage "<remove_user_data>")}"

  nb_instances="$(_signal_multi_instances__list_data_dir | wc -l)"
  nb_instances_to_remove="$(_signal_multi_instances__list_removed_instances | wc -l)"

  if [ "${nb_instances}" -eq 0 ]; then
    gettext $"You currently have no other instance than the default one."
    echo
  else
    eval_ngettext $"You currently have a unique instance:" $"You currently have ${nb_instances} different instances:" "${nb_instances}"
    echo
    _signal_multi_instances__list_data_dir | sed 's/^/- /'
  fi

  if [ "${nb_instances_to_remove}" -gt 0 ]; then
    if [ "${remove_user_data}" = "true" ]; then
      for data_dir in $(_signal_multi_instances__list_removed_instances); do
        log 4 "$(eval_gettext $"Removing instance data in '${data_dir}'")"
        rm -rf "${data_dir}"
      done
    else
      if [ "${__VERBOSE}" -ge 4 ]; then
        log 4 "$(gettext $"Maybe you want to remove manually those files:")"
        echo -ne "${S_ITALIC}"
        _signal_multi_instances__list_removed_instances | sed 's/^/    rm -rf /'
        echo -ne "${RESET}"
      fi
    fi
  fi
}

# shellcheck disable=SC2120
signal_multi_instances__create() {
  local instance="${1:?$(usage "<instance_name>")}"
  instance="$(_signal_multi_instances__sanitizer "${instance}")"

  log 3 "$(eval_gettext $"Creating the new Signal instance '${instance}'")"

  local desktop_name="${DIR_APPS}/signal-${OS_NAME,,}-${instance}.desktop"
  if [ -f "${desktop_name}" ] || [[ ${desktop_name} == "TAILS" ]]; then
    log 2 "$(eval_gettext $"The Signal instance already exists: '${instance}'")"
    return
  fi
  mkdir -p "${dir_config}/Signal-${instance}"

  export SIGNALID="${instance}"
  envsubst <"${DIR_FILES}/Desktop/signal-${OS_NAME,,}-template.desktop" >"${desktop_name}"

  if OS_is_TAILS; then
    log 5 "$(eval_gettext $"Copying '${desktop_name}' into '${DIR_APPS_PERSISTENT}'")"
    cp "${desktop_name}" "${DIR_APPS_PERSISTENT}/"
  fi
}

signal_multi_instances__rename() {
  print_usage() {
    usage "<id_old> <id_new>"
  }

  local id_old="${1:?$(print_usage)}"
  local id_new="${2:?$(print_usage)}"

  id_old="$(_signal_multi_instances__sanitizer "${id_old}")"
  id_new="$(_signal_multi_instances__sanitizer "${id_new}")"

  log 3 "$(eval_gettext $"Renaming Signal instance '${id_old}' into '${id_new}'")"

  local desktop_name_old="${DIR_APPS}/signal-${OS_NAME,,}-${id_old}.desktop"
  local desktop_name="${DIR_APPS}/signal-${OS_NAME,,}-${id_new}.desktop"
  local data_old="${dir_config}/Signal-${id_old}"
  local data_new="${dir_config}/Signal-${id_new}"

  if [ ! -f "${desktop_name_old}" ] || [ ! -d "${data_old}" ]; then
    log 2 "$(eval_gettext $"Error while renaming the instance '${id_old}' into '${id_new}', '${id_old}' does not exist")"
    return
  fi
  if [ -f "${desktop_name}" ] || [ -d "${data_new}" ]; then
    log 2 "$(eval_gettext $"Error while renaming the instance '${id_old}' into '${id_new}', '${id_new}' already exist")"
    return
  fi

  mv "${desktop_name_old}" "${desktop_name}"
  sed -i "s/Signal-${id_old}/Signal-${id_new}/" "${desktop_name}"

  mv "${data_old}" "${data_new}"

  if OS_is_TAILS; then
    log 5 "$(eval_gettext $"Copying '${desktop_name}' into '${DIR_APPS_PERSISTENT}'")"
    rm -f "${DIR_APPS_PERSISTENT}/signal-${OS_NAME,,}-${id_old}.desktop"
    cp "${desktop_name}" "${DIR_APPS_PERSISTENT}/"
  fi
}

signal_multi_instances__clean() {
  print_usage() {
    usage "<instance_name> <remove_user_data>"
  }

  local instance="${1:?$(print_usage)}"
  local remove_user_data="${2:?$(print_usage)}"

  instance="$(_signal_multi_instances__sanitizer "${instance}")"

  log 3 "$(eval_gettext $"Cleaning Signal instance '${instance}'")"

  rm -f "${DIR_APPS}/signal-${OS_NAME,,}-${instance}.desktop"

  if OS_is_TAILS; then
    rm -f "${DIR_APPS_PERSISTENT}/signal-${OS_NAME,,}-${instance}.desktop"
  fi

  local data_dir="${dir_config}/Signal-${instance}"
  if [ -d "${data_dir}" ]; then
    log 4 "$(eval_gettext $"Removing instance data in '${data_dir}'")"
    if [ "${remove_user_data}" = "true" ]; then
      rm -rf "${data_dir}"
    else
      log 4 "$(eval_gettext $"The data located in '${data_dir}' were not removed")"
    fi
  fi
}

signal_multi_instances__regenerate() {
  print_usage() {
    usage "<instance_name>"
  }

  local instance="${1:?$(print_usage)}"
  instance="$(_signal_multi_instances__sanitizer "${instance}")"

  local desktop_name="${DIR_APPS}/signal-${OS_NAME,,}-${instance}.desktop"
  if [ ! -f "${desktop_name}" ]; then
    log 2 "$(eval_gettext $"Error while regenerating desktop file of the instance '${instance}', it does not exist")"
    return
  fi

  log 3 "$(eval_gettext $"Regenerating desktop file of Signal instance '${instance}'")"

  export SIGNALID="${instance}"
  envsubst <"${DIR_FILES}/Desktop/signal-${OS_NAME,,}-template.desktop" >"${desktop_name}"

  if OS_is_TAILS; then
    cp "${desktop_name}" "${DIR_APPS_PERSISTENT}/"
  fi
}
