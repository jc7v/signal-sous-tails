#!/usr/bin/env bash

# Exit on error
set -euo pipefail

# shellcheck source-path=src/lib
source "$(dirname -- "${BASH_SOURCE[0]}")/utils.sh"

##### SIGNAL CAPTCHAS HANDLER ##########################################################################################
signal_captchas__get_version() {
  result="${STR_NOT_FOUND}"
  if [ -s "${DIR_APPS}/${STR_CAPTCHA_HANDLER_NAME}.desktop" ] && [ -s "${dir_bin}/${STR_CAPTCHA_HANDLER_NAME}.sh" ]; then
    result="${STR_INSTALLED}"
  fi

  local tool_name="${STR_TOOL_CAPTCHAS}"
  log 5 "$(eval_gettext $"Tool '${tool_name}' has currently the version '${result}'")"

  echo "${result}"
}

signal_captchas__any() {
  local tool_name="${STR_TOOL_CAPTCHAS}"
  log 3 "$(eval_gettext $"Installation of the tool '${tool_name}'")"

  cp "${DIR_FILES}/Desktop/${STR_CAPTCHA_HANDLER_NAME}.desktop" "${DIR_APPS}/"
  cp "${DIR_FILES}/Scripts/${STR_CAPTCHA_HANDLER_NAME}.sh" "${dir_bin}/"

  if grep -q "signalcaptcha" "${SIGNAL_DESKTOP_DESKTOP_FILE}" 2>/dev/null; then
    log 4 "$(eval_gettext $"${tool_name} will now handle multiple instances, you can roll back from file '${SIGNAL_DESKTOP_DESKTOP_FILE}.backup'")"
    _sudo cp "${SIGNAL_DESKTOP_DESKTOP_FILE}" "${SIGNAL_DESKTOP_DESKTOP_FILE}.backup"
    _sudo sed -i 's#^MimeType=.*#MimeType=x-scheme-handler/sgnl;#g' "${SIGNAL_DESKTOP_DESKTOP_FILE}"
  fi
}

signal_captchas__tails() {
  local signal_desktop="${DIR_APPS_PERSISTENT}/Signal.desktop"

  if [ -f "${signal_desktop}" ]; then
    if grep -q "signalcaptcha" "${signal_desktop}"; then
      log 5 "$(gettext $"Removing the signalcaptchas handler scheme from Signal Desktop")"
      cp "${signal_desktop}" "${signal_desktop}.backup"
      sed -i 's#^MimeType=.*#MimeType=x-scheme-handler/sgnl;#' "${signal_desktop}"
      cp "${signal_desktop}" "${DIR_APPS}/"
    fi
  fi

  sed -i "s#^Exec=.*#Exec=${dir_bin}/${STR_CAPTCHA_HANDLER_NAME}.sh %U#g" "${DIR_APPS}/${STR_CAPTCHA_HANDLER_NAME}.desktop"

  # == Making it functional even after reboot == #
  local tool_name="${STR_TOOL_CAPTCHAS}"
  log 5 "$(eval_gettext $"Making '${tool_name}' functional even after Tails reboot")"
  cp "${DIR_APPS}/${STR_CAPTCHA_HANDLER_NAME}.desktop" "${DIR_APPS_PERSISTENT}/"
  cp "${DIR_FILES}/Scripts/${STR_CAPTCHA_HANDLER_NAME}.sh" "${DIR_BIN_PERSISTENT}/"
}

signal_captchas() {
  signal_captchas__any

  if OS_is_Debian; then
    :
  elif OS_is_TAILS; then
    signal_captchas__tails
  else
    fail_not_supported "${STR_TOOL_CAPTCHAS}"
  fi

  log 3 "${STR_TOOL_CAPTCHAS} $(gettext $"installed")"
}

signal_captchas__update() {
  local tool_name="${STR_TOOL_CAPTCHAS}"
  log 3 "$(eval_gettext $"Updating the tool '${tool_name}'")"

  signal_captchas
}

signal_captchas__clean() {
  local tool_name="${STR_TOOL_CAPTCHAS}"
  log 3 "$(eval_gettext $"cleaning tool '${tool_name}'")"

  rm -f "${DIR_APPS}/${STR_CAPTCHA_HANDLER_NAME}.desktop"
  rm -f "${dir_bin}/${STR_CAPTCHA_HANDLER_NAME}.sh"

  if OS_is_Debian; then
    if [ -f "${SIGNAL_DESKTOP_DESKTOP_FILE}.backup" ]; then
      log 5 "$(eval_gettext $"Recovery of '${SIGNAL_DESKTOP_DESKTOP_FILE}' from backup")"
      _sudo mv "${SIGNAL_DESKTOP_DESKTOP_FILE}.backup" "${SIGNAL_DESKTOP_DESKTOP_FILE}"
    fi
  elif OS_is_TAILS; then
    local signal_desktop="${DIR_APPS_PERSISTENT}/Signal.desktop"

    if [ -f "${signal_desktop}.backup" ]; then
      log 5 "$(eval_gettext $"Recovery of '${signal_desktop}' from backup")"
      mv "${signal_desktop}.backup" "${signal_desktop}"
    fi
    rm -f "${DIR_APPS_PERSISTENT}/${STR_CAPTCHA_HANDLER_NAME}.desktop"
    rm -f "${DIR_BIN_PERSISTENT}/${STR_CAPTCHA_HANDLER_NAME}.sh"
  fi
}
