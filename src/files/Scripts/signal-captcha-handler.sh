#!/usr/bin/env bash

set -euo pipefail

# Une fois un captcha résolu, ce script permet à l'utilisateur⋅rice de choisir l'instance Signal à débloquer

##################
# Tails ou non ? #
##################

dir_config="${HOME}/.config"
bin_signal="/opt/Signal/signal-desktop"
command=()

if [[ "$(awk -F= '/^NAME/{print $2}' /etc/os-release)" == '"Tails"' ]]; then
  command+=(env HTTP_PROXY='socks://127.0.0.1:9050' HTTPS_PROXY='socks://127.0.0.1:9050')

  dir_config="${HOME}/Persistent/.config"
  command+=("${bin_signal}" --no-sandbox --password-store="basic")
else
  command+=("${bin_signal}" --no-sandbox)
fi

###########################
# Récupération du captcha #
###########################

captcha=""
if [ "$#" -eq 0 ]; then
  captcha="$(zenity --entry --text "Captcha" --entry-text "signalcaptcha://" 2>/dev/null)"
else
  captcha="${1}"
  shift
fi

list_instances=()
while IFS= read -r -d $'\0' REPLY; do
  bool_value="FALSE"
  if [[ ${REPLY} == "${dir_config}/Signal" ]]; then
    bool_value="TRUE"
  fi
  list_instances+=("${bool_value}" "${REPLY}")
done < <(find "${dir_config}" -name "Signal*" -print0 | sort -z)
dir_signal_config="$(zenity --list --radiolist --width 600 --height 450 --text "Pour quelle instance Signal le captcha a-t-il été résolu ?" --column "" --column "" --hide-header "${list_instances[@]}" 2>/dev/null)"

command+=(--user-data-dir="${dir_signal_config}" "${captcha}")

echo "$(date) $0 called; command='${command[*]}'" >>/tmp/hack.log

########################################
# Envoi du captcha à la bonne instance #
########################################

# shellcheck disable=SC2090
"${command[@]}" "$@"
