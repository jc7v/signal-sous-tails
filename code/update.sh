#! /usr/bin/env bash

wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor >signal-desktop-keyring.gpg
sudo bash -c "
cat signal-desktop-keyring.gpg | tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] tor+https://updates.signal.org/desktop/apt xenial main' |\
tee -a /etc/apt/sources.list.d/signal-xenial.list
apt update
"
apt download signal-desktop
mkdir -p ~/Applications/signal-desktop
dpkg-deb -xv $(ls signal-desktop*.deb) ~/Applications/signal-desktop

cp -r ~/Applications/ /live/persistence/TailsData_unlocked/dotfiles/
echo "Signal a été mis à jour!"
