#!/usr/bin/env sh

# Generate locales files from PO translations file

for pofile in "$@"; do
  echo "Generating MO file from '${pofile}'"
  mofile="${pofile%.*}.mo"

  msgfmt "${pofile}" -o "${mofile}"
  git add "${mofile}"
done
