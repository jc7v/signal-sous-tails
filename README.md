# Outils autour de Signal

Ce répertoire a pour objectif de regrouper et d'automatiser des outils pour mieux utiliser Signal.

Il fournit également de la documentation pour installer/utiliser Signal, notamment sous [Tails](https://tails.net) et sans Smartphone.

## Vue d'ensemble des outils Signal

Le script [`src/signal-tools`](https://0xacab.org/jc7v/signal-sous-tails/blob/main/src/signal-tools) permet d'installer les outils suivants :

* **Signal Tools** : Gestionnaire d'outils autour de Signal
* **Signal Desktop** : Application pour accéder à ses messages Signal sur son ordinateur
* **Signal CLI** : Outil pour configurer Signal sans avoir de smartphone
* **Additional Signal instances** : Crée de nouvelles instances Signal pour avoir plusieurs numéros liés
* **Signal Captchas Handler** : Si une instance demande la résolution d'un captcha, permet depuis le navigateur de rouvrir la bonne instance

Les systèmes d'exploitation suivants sont actuellement compatibles avec le script :

* [**TAILS**](https://tails.net)
* [**Ubuntu 24.04 LTS**](https://ubuntu.com/download/desktop)
* [**Debian 12**](https://www.debian.org/)

## Documentation

### Pré-requis

> Signal demande que les comptes soient régulièrement consultés.
> Il y a une suspicion de risque de fermeture d'un compte en cas de non utilisation prolongée.
> Dans le cas d'un compte créé avec une carte SIM «jetable», le compte sera dans ce cas définitivement perdu.
> Il est donc conseillé de consulter ses messages régulièrement (de l'ordre de tous les 15 jours a minima).

* Une connaissance minimale de l'usage du terminal (ce tutoriel essaie d'expliciter chaque manipulation mais ça reste délicat sans aucune expérience)
* Un numéro de téléphone jetable (nommé +336XXXXXXXX dans ce tuto, en cas de numéro en 07, le numéro à renseigner sera sous la forme +337XXXXXXXX), sur lequel recevoir un SMS ou un appel. Il est bon de rappeler que les opérateurs gardent en mémoire l'identifiant du téléphone (IMEI) en communication et l'associent à la carte SIM utilisée. Pour être tout à fait anonyme (ou disons plus anonyme), il faut donc :

 1. Utiliser un boîtier téléphonique jetable. Par exemple, un téléphone à touche que l’on trouve en grande surface pour 10-20€).
 2. Réaliser l'opération ailleurs que chez soi. Attention : l’activation d'une nouvelle carte SIM peut prendre plusieurs minutes.
 3. Bien évidemment, une carte SIM ; LycaMobile ou SymaMobile se trouvent dans de nombreux tabacs et elles sont utilisables pendant deux semaines avant de devoir s’enregistrer. Pour qu’elles soient activées, il est nécessaire d’ajouter du crédit.

### Spécifique à [Tails](https://tails.net)

1. Les appels audio et vidéo ne fonctionnent pas avec cette méthode (et à priori pas avec Tor)
2. La sécurité de Tails sera abaissée étant donné que le code de signal-cli et Signal Desktop n’est pas vérifié avec la même rigueur que le reste du code de Tails et Debian. Néanmoins, c’est du code open-source, lu et vérifié par de nombreuses personnes compétentes à travers le monde.

Merci aux blogs bisco.org et ctrl.alt.coop pour les tutos, à ??? dont ce tuto est très fortement inspiré, et à AsamK pour signal-cli

1. Démarrer l'ordinateur sur la clef Tails
2. Déverrouiller la persistance
3. Définir un mot de passe d'administration
4. Configurer la persistance
  * Lancer l’application *`Configurer le volume persistent`*
  * Cocher les options : *`Dotfiles`* et *`Logiciels supplémentaires`*

### Installation de `Signal Tools`

1. [Télécharger le script d'installation](https://0xacab.org/jc7v/signal-tools/-/raw/main/install.sh?ref_type=heads&inline=false)
2. Ouvrir le dossier des Téléchargements dans le navigateur de fichiers
3. Clique-droit sur le fichier `install.sh` et choisir **`Exécuter comme un programme`**
4. Rentrer le mot de passe administrateur quand c'est demandé dans le terminal. Si rien ne s'affiche pendant que vous le tappez, ne vous inquiétez pas ; écrivez votre mot de passe en entier et finir par *Entrée*

### Utilisation de l'interface graphique

1. Lancer l'application **`Application > Autre > Signal Tools`**
2. Se laisser guider pour installer les outils qui vous intéressent

### Utilisation en ligne de commande

*Optez pour cette option uniquement si vous vous sentez à l'aise avec*

1. Ouvrir un terminal
2. Exécuter `signal-tools --help`, vous devriez voir le texte suivant :

  ```
Usage: ./signal-tools  {-g | --gui}         Launch the Graphical User Interface
Usage: ./signal-tools [OPTION...] [COMMAND] [ARGUMENTS...]
      OPTIONS SUMMARY
             -h | --help                    Print help information
             -v | --verbose                 Increase verbosity
             -q | --quiet                   Suppress non-error messages
             -n | --notification            The log messages are displayed as notifications
             -R | --remove-data             Remove also user data
      COMMANDS SUMMARY
             -i | --install  {all | captchas | cli | desktop}
             -r | --remove   {all | captchas | cli | desktop}
             -V | --version  {all | captchas | cli | desktop}
             -u | --update                  Update all tools
             -l | --list-instances          List all Signal instances
             -a | --add-instance  <ID>      Add a new Signal instance
             -d | --drop-instance <ID>      Remove an existing Signal instance
  ```

- Si vous voulez avoir le maximum de logs en installant **Signal Desktop**, **Signal Captchas** et créer une nouvelle instance Signal nommée *Perso* :

  ```bash
  signal-tools --install desktop --install captchas --add-instance Perso
  ```

- Si vous voulez connaître la version des outils installés et avoir la liste de toutes les instances Signal :

  ```bash
  signal-tools --version all --list-instances
  ```

- Pour mettre à jour l'ensemble des outils installés :

  ```bash
  signal-tools --update
  ```

## Comment contribuer

- Proposition d'environnement de travail
  - IDE : vscode, jetbrains/pycharm ou selon vos préférences
  - Virtualisation d'OS ([virt-manager](https://virt-manager.org/)) : [Tails](https://tails.net/doc/advanced_topics/virtualization/virt-manager/index.en.html) pour les tests, [Whonix](https://www.whonix.org/wiki/KVM) pour publier, `Dossier partagé` entre les VMs
- Garder une base de code saine avec [pre-commit](https://pre-commit.com/) :
  - Installation : `pip install pre-commit`
  - Une fois le dépôt cloné, exécuter `pre-commit install`
  - À chaque commit, un ensemble de scripts sera exécuté pour garantir une homogénéité du code
- **Traduction**
  - Exécuter le script 'src/generate_po.sh` pour générer le fichier de traductions à partir des chaînes à traduire. Le script prend en argument les noms des scripts à traduire et remplis le fichier `src/translations/<lang>/LC_MESSAGES/messages.po`
  - Avec le logiciel `gtranslator`, traduire l'ensemble des messages en attente
  - Exécuter le script `src/generate_mo.sh src/translations/<lang>/LC_MESSAGES/messages.po` pour compiler les traductions et les ajouter au git

## FAQ

- J'ai trouvé un bug, comment le faire remonter ?

  > Ouvrir une issue : https://0xacab.org/jc7v/signal-tools/-/issues

## Legacy

Pour savoir comment utiliser ces outils sous TAILS, [voici le lien vers la visualisation de la documentation](https://0xacab.org/jc7v/signal-sous-tails/blob/main/docs/signal-sous-tails-v2.md)
