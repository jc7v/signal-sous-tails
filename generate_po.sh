#!/usr/bin/env bash

# Generate PO files from translate strings

set -euo pipefail

DIR_ROOT=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

supported_languages=(fr)

xgettext_wrapper() {
  script="${1:?script}"
  pofile="${2:?pofile}"
  shift 2

  xgettext_args=(--force-po --no-wrap --sort-by-file --join-existing --omit-header --from-code=utf-8 -L Shell)
  xgettext_args+=(-o "${pofile}")
  xgettext "${xgettext_args[@]}" "${script}" "$@" 2> >(grep -v " is deprecated due to security reasons; use eval_gettext instead" >&2) || echo "XGETTEXT FAIL FOR FILE '${script}'"
}

for language in "${supported_languages[@]}"; do
  dir_translations="${DIR_ROOT}/src/translations/${language}/LC_MESSAGES"
  mkdir -p "${dir_translations}"

  pofile="${dir_translations}/messages.po"
  touch "${pofile}"

  xgettext_wrapper "${DIR_ROOT}/src/signal-tools" "${pofile}" --no-location

  while IFS= read -r -d '' script; do
    script="${script#"${DIR_ROOT}/"}"
    echo "Generation of locales from '${script}'"
    xgettext_wrapper "${script}" "${pofile}"
  done < <(find "${DIR_ROOT}/src" -type f -executable -print0)
done
