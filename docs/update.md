# Mise à jour de Signal sous Tails

1. Si l'ordinateur est allumé, le redémarrer
2. Quand l'écran d'accueil apparaît, choisir la langue, déverouiller la persistance, puis cliquer sur paramètres supplémentaires et choisir *Mot de passe d'administration*. Définir un mot de passe d'administration
3. Cliquer sur démarrer Tails
4. Se connecter à un réseau Wifi au filaire
5. Quand la fenêtre *Connexion à Tor* apparaît, se connecter à Tor
6. Lancer le naviateur Tor et télécharger le script de mise à jour depuis cette adresse: https://0xacab.org/jc7v/signal-sous-tails/-/raw/master/code/update.sh?inline=false
6. Enregistrer le script dans le dossier "Tor Browser"
7. Cliquer sur *Emplacements* (barre du haut de l'écran à gauche), puis sur *Tor Browser*
8. Dans la fenêtre principale de l'exporateur de fichier qui s'est ouvert, clique-droit sur du vide (ne pas cliquer sur un fichier et dossier) et choisir *Ouvrir dans un terminal*
8. S'assurer que Signal Desktop ne soit pas lancé.
8. Copier la ligne suivante et la coller dans le terminal:

        bash update.sh

9. Quand demandé, rentrer le mot de passe d'aministration défini au point 2 (c'est normal que rien ne s'affiche quand vous tapez le mot de passe)
10. Attendre jusqu'à ce que tout en bas du terminal s'affiche *Signal a été mis à jour!*

Et voilà! Vous pouvez lancer Signal et commencer à l'utiliser
